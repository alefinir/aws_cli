#!/bin/bash
##########
##Save day of the week
DIA=`date +%A`
##Save date into a variable
FECHA=`date +%A-%Y%m%d`
##update log file
echo "fecha=$FECHA" >> /task/backup_instances/log.txt
echo "dia=$DIA" >> /task/backup_instances/log.txt
##execute ansible playbook with tags (day of the week and date)
/usr/bin/ansible-playbook /task/backup_instances/backup.yml --extra-vars "fecha=$FECHA periodo=$DIA" >> /task/log.txt
