# Bash scripts with ansible and aws cli to perform backups and delete amis older than 30 days

These two scripts work together, the first one creates the backups using a given tag, the delete script searches all the amis with that tag, verifies that they are older than 30 days, deregister them and deletes the associated snapshops.

---

## Backup with ansible

* The bash script calls an ansible playbook with 2 parameters (day of the week and date)
* The ansible playbook use aws login credentials and then set tags for new snapshots (day of the week, date and domain name)

It is very important to set every instance id with the format i-XXXXXXXXXXXXXXXXX with your real instances id, name tag and regions.

Also I provide an example of crontab file that runs everyday at 1:00 am.


## Bash Script to delete older amis

* `delamisName.sh` receives a string as a parameter and then search all the amis associated with this string, after that, all the amis IDs found are saved in a temporary file.
* `delsnap.sh` looks for creation's date of every ami, if this is older than 30 days, the ami is deregistered and then every snapshot associated is deleted. 
