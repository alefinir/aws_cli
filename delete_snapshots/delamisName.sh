#!/bin/bash
##########
#---------credentials
export AWS_ACCESS_KEY_ID=XXXXX
export AWS_SECRET_ACCESS_KEY=XXXXXXXXX
#---------set region the second parameter
export AWS_DEFAULT_REGION="$2"
export AWS_CONFIG_FILE=text

#looks for the name in instance tags
INSTANCENAME="$1"
INSTANCENAME="*"$INSTANCENAME"*"

#echo "$INSTANCENAME"

aws ec2 describe-images --output text --filters "Name=name,Values=$INSTANCENAME" --query 'Images[*].{ID:ImageId}' > /tmp/amis.txt

#the script delete every snapshot from this instance

for i in `cat /tmp/amis.txt`;do bash /task/borrado/delsnap.sh "$i" "$2"; done
