#!/bin/bash
##########
#---------credentials
export AWS_ACCESS_KEY_ID=XXXXXX
export AWS_SECRET_ACCESS_KEY=XXXXXXXXX
export AWS_DEFAULT_REGION="$2"
export AWS_CONFIG_FILE=text

####
#get the date of creation of the ami 
####

tempTime=`aws ec2 describe-images --image-ids "$1"  --query 'Images[*].[ImageId,CreationDate]' --output text | awk '{ print $2 }'`
amiDate=$(echo ${tempTime:0:10} | tr -d "-")
today_var=`date +%Y%m%d`

#echo "AMI Fecha: $amiDate"

DIFFERENCE=$(( ($(date --date "$today_var" +%s) - $(date --date "$amiDate" +%s) )/(60*60*24) ))


#check 30 days 

if [[ "$DIFFERENCE" -gt 30 ]];
then
        echo "Deleting $1 of the region $2"
        #Find snapshots associated with AMI.
        aws ec2 describe-images --image-ids "$1" | grep snap | awk ' { print $2 }'  > /tmp/tmp.txt

        #delete ,"; characters
        cat /tmp/tmp.txt | tr -d "," | tr -d ";" | tr -d "\"" > /tmp/snap.txt

        #echo -e "Starting the Deregister of AMI... \n"

        #Deregistering the AMI 
        aws ec2 deregister-image --image-id "$1"

        #echo -e "\nDeleting the associated snapshots.... \n"

        #Deleting snapshots attached to AMI
        for i in `cat /tmp/snap.txt`;do aws ec2 delete-snapshot --snapshot-id $i ; done

else
        echo "The difference it is not enought, days: $DIFFERENCE" 
fi
